#ifndef __PRINT_IN_COLOR_H_
#define __PRINT_IN_COLOR_H_

#ifdef __PRINT_IN_COLOR__
    #define TEXT_RESET				TEXT_FORAMT_RESET "" TEXT_COLOR_RESET "" TEXT_BACK_RESET

    #define TEXT_FORAMT_RESET		"\033[0m"
    #define TEXT_FORAMT_BOLD		"\033[1m"
    #define TEXT_FORAMT_DIM			"\033[2m"
    #define TEXT_FORAMT_USCORE		"\033[4m"
    #define TEXT_FORAMT_BLINK		"\033[5m"
    #define TEXT_FORAMT_REVERSE		"\033[7m"
    #define TEXT_FORAMT_HIDDEN		"\033[8m"

    #define TEXT_COLOR_BLACK		"\033[30m"
    #define TEXT_COLOR_RED			"\033[31m"
    #define TEXT_COLOR_GREEN		"\033[32m"
    #define TEXT_COLOR_YELLOW		"\033[33m"
    #define TEXT_COLOR_BLUE			"\033[34m"
    #define TEXT_COLOR_MAGENTA		"\033[35m"
    #define TEXT_COLOR_CYAN			"\033[36m"
    #define TEXT_COLOR_WHITE		"\033[37m"
    #define TEXT_COLOR_RESET		"\033[39m"

    #define TEXT_BACK_BLACK			"\033[40m"
    #define TEXT_BACK_RED			"\033[41m"
    #define TEXT_BACK_GREEN			"\033[42m"
    #define TEXT_BACK_YELLOW		"\033[43m"
    #define TEXT_BACK_BLUE			"\033[44m"
    #define TEXT_BACK_MAGENTA		"\033[45m"
    #define TEXT_BACK_CYAN			"\033[46m"
    #define TEXT_BACK_WHITE			"\033[47m"
    #define TEXT_BACK_RESET			"\033[49m"
#else
    #define TEXT_RESET

    #define TEXT_FORAMT_RESET
    #define TEXT_FORAMT_BOLD
    #define TEXT_FORAMT_DIM
    #define TEXT_FORAMT_USCORE
    #define TEXT_FORAMT_BLINK
    #define TEXT_FORAMT_REVERSE
    #define TEXT_FORAMT_HIDDEN

    #define TEXT_COLOR_BLACK
    #define TEXT_COLOR_RED
    #define TEXT_COLOR_GREEN
    #define TEXT_COLOR_YELLOW
    #define TEXT_COLOR_BLUE
    #define TEXT_COLOR_MAGENTA
    #define TEXT_COLOR_CYAN
    #define TEXT_COLOR_WHITE
    #define TEXT_COLOR_RESET

    #define TEXT_BACK_BLACK
    #define TEXT_BACK_RED
    #define TEXT_BACK_GREEN
    #define TEXT_BACK_YELLOW
    #define TEXT_BACK_BLUE
    #define TEXT_BACK_MAGENTA
    #define TEXT_BACK_CYAN
    #define TEXT_BACK_WHITE
    #define TEXT_BACK_RESET
#endif

#endif // __PRINT_IN_COLOR_H_